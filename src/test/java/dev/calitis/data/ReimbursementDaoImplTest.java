package dev.calitis.data;

import dev.calitis.model.Reimbursement;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ReimbursementDaoImplTest {
    ReimbursementDaoImpl reimbursementDao = new ReimbursementDaoImpl();

    @Test
    void getReimbursmentByEmployeeID() {
    }

    @Test
    void createNewReimbursement() {
    }

    @Test
    void getAllReimbursement() {
    }

    @Test
    void getAllPendingReimbursement() {
    }

    @Test
    void getAllPendingReimbursementById() {
    }

    @Test
    void getAllApprovedReimbursementById() {
    }

    @Test
    void getAllRejectedReimbursementById() {
    }

    @Test
    void getAllResolvedReimbursement() {
    }
}