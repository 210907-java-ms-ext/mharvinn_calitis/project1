document.getElementById("LogOut").addEventListener("click",logout);
document.getElementById("Show-Employee-Reimbursement").addEventListener("click",showEmployeeReimbursement);
document.getElementById("Show-Pending-Reimbursement").addEventListener("click",showPendingReimbursement);
document.getElementById("Show-Approved-Reimbursement").addEventListener("click",showApprovedReimbursement);
document.getElementById("Show-Rejected-Reimbursement").addEventListener("click",showRejectedReimbursement);
document.getElementById("Show-Employee-Profile").addEventListener("click",showEmployeeProfile);
document.getElementById("submit-btn").addEventListener("click",createNewReimbursement);

const reimbursementShow = document.getElementById("reimbursementTable");
const employeeShow = document.getElementById("Employee-Profile");

async function logout(){

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/project1/logout");
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4){
            console.log(localStorage);
            localStorage.removeItem("token");
            console.log(localStorage);
            window.location.href = "/project1/login.html"
        }
    }
    xhr.send();
  
}

function showEmployeeReimbursement(){
    const xhr = new XMLHttpRequest();
    const token =  window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "ID";


    xhr.open("POST", "/project1/reimbursement");
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4){
            let reimbursementList = JSON.parse(xhr.responseText);
            reimbursementList2 = JSON.parse(xhr.responseText);
            appendReimbursementToDom(reimbursementList);
        }
    }
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    const requestBody = `employee_id=${account[0]}&type=${type}`;
    console.log(requestBody);
    xhr.send(requestBody);
}
function showPendingReimbursement(){
    console.log("click");
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "ID-PEN"

        xhr.open("POST", "/project1/reimbursement");
        xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                let reimbursementList = JSON.parse(xhr.responseText);
                reimbursementList2 = JSON.parse(xhr.responseText);
                appendReimbursementToDom(reimbursementList);
            }
        }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `employee_id=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);
}

function showApprovedReimbursement(){
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "ID-APP"

        xhr.open("POST", "/project1/reimbursement");
        xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                let reimbursementList = JSON.parse(xhr.responseText);
                reimbursementList2 = JSON.parse(xhr.responseText);
                appendReimbursementToDom(reimbursementList);
            }
        }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `employee_id=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);

}

function showRejectedReimbursement(){
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "ID-REJ"

        xhr.open("POST", "/project1/reimbursement");
        xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                let reimbursementList = JSON.parse(xhr.responseText);
                reimbursementList2 = JSON.parse(xhr.responseText);
                appendReimbursementToDom(reimbursementList);
            }
        }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `employee_id=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);
}
function createNewReimbursement(e){
    e.preventDefault();
    const amount = document.getElementById("amount").value;
    const description = document.getElementById("description").value;
    console.log(amount)
    console.log(description)
    if(amount == 0 || description == ""){
        document.getElementById("error").hidden = false;
    }else{
        document.getElementById("error").hidden = true;
        const xhr = new XMLHttpRequest();
        const token = window.localStorage.getItem("token");
        const account = token.split(':');
        const type = "ID-CREATE"

        xhr.open("POST", "/project1/reimbursement");
        xhr.onreadystatechange = function(){
         if(xhr.readyState===4){
            window.location.reload();
            }
         }
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            const requestBody = `description=${description}&amount=${amount}&employee_id=${account[0]}&type=${type}`;
            console.log(requestBody);
            xhr.send(requestBody);
        }
}

function showEmployeeProfile(){
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "EMP-ID"
    xhr.open("POST", "/project1/employee");
    xhr.onreadystatechange = function(){
     if(xhr.readyState===4){
            let employee = JSON.parse(xhr.responseText);
            let employeeList = employee;
            showProfile(employee);
        }
     }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `employee_id=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);

}
function showProfile(employee){
    reimbursementShow.hidden = true;
    employeeShow.hidden = false;
    document.getElementById("tprofile").innerHTML='';

    let employeeTable = document.getElementById("tprofile");

     let row = document.createElement("tr");
     row.className ="table-dark";
     let employeeId = document.createElement("td");
     employeeId.innerHTML = employee.employeeId;
     row.appendChild(employeeId);
     let employeeFName = document.createElement("td");
     employeeFName.innerHTML = employee.employeeFName;
     row.appendChild(employeeFName);
     let employeeLName = document.createElement("td");
     employeeLName.innerHTML = employee.employeeLName;
     row.appendChild(employeeLName);
     let employeeEmail= document.createElement("td");
     employeeEmail.innerHTML = employee.employeeEmail;
     row.appendChild(employeeEmail);
     employeeTable.appendChild(row);

}

function appendReimbursementToDom(reimbursementList){
    document.getElementById('tbody').innerHTML = '';

    employeeShow.hidden = true;
    reimbursementShow.hidden = false;

    let reimbursementTable = document.getElementById("tbody");

    for (let reimbursement of reimbursementList) {
        let row = document.createElement("tr");
        row.className ="table-dark";
        let reimbursementId = document.createElement("td");
        reimbursementId.innerHTML = reimbursement.reimbursementId;
        row.appendChild(reimbursementId);
        let reimbursementAmount = document.createElement("td");
        reimbursementAmount.innerHTML = reimbursement.amount;
        row.appendChild(reimbursementAmount);
        let reimbursementInfo = document.createElement("td");
        reimbursementInfo.innerHTML = reimbursement.reimbursementInfo;
        row.appendChild(reimbursementInfo);
        let employee = document.createElement("td");
        employee.innerHTML = reimbursement.employee.employeeFName;
        row.appendChild(employee);
        let status = document.createElement("td");
        status.innerHTML = reimbursement.status;
        row.appendChild(status);
        reimbursementTable.appendChild(row);
    }

}

