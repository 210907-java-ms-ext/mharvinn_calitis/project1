document.getElementById("Show-All-Employees").addEventListener("click",getAllEmployees);
document.getElementById("Show-All-Reimbursement").addEventListener("click",getAllReimbursements);
document.getElementById("Show-All-Pending").addEventListener("click",getAllPending);
document.getElementById("Show-All-Approved").addEventListener("click",getAllApproved);
document.getElementById("Show-All-Rejected").addEventListener("click",getAllRejected);
document.getElementById("LogOut").addEventListener("click",logout);
document.getElementById("submit-btn").addEventListener("click",approveDeny);

const reimbursementShow = document.getElementById("reimbursementTable");
const employeeShow = document.getElementById("Employee-Profile");
let reimbursementList2;

async function logout(){

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/project1/logout");
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4){
            console.log(localStorage);
            localStorage.removeItem("token");
            console.log(localStorage);
            window.location.href = "/project1/login.html"
        }
    }
    xhr.send();

}

function getAllEmployees(){
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "EMP-ALL"
    xhr.open("POST", "/project1/employee");
    xhr.onreadystatechange = function(){
     if(xhr.readyState===4){
            let employeeList = JSON.parse(xhr.responseText);
            showProfile(employeeList);
        }
     }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `employee_id=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);

}

function getAllReimbursements(){
    const xhr = new XMLHttpRequest();
    const token =  window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "ALL";


    xhr.open("POST", "/project1/reimbursement");
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4){
        console.log(xhr.responseText);
            let reimbursementList = JSON.parse(xhr.responseText);
            reimbursementList2 = JSON.parse(xhr.responseText);
            appendReimbursementToDom(reimbursementList);
        }
    }
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    const requestBody = `type=${type}`;
    console.log(requestBody);
    xhr.send(requestBody);

}

function getAllPending(){
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "PEN-ALL"

        xhr.open("POST", "/project1/reimbursement");
        xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                let reimbursementList = JSON.parse(xhr.responseText);
                reimbursementList2 = JSON.parse(xhr.responseText);
                appendReimbursementToDom(reimbursementList);
            }
        }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `account=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);

}

function getAllApproved(){
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "APP-ALL"

        xhr.open("POST", "/project1/reimbursement");
        xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                let reimbursementList = JSON.parse(xhr.responseText);
                reimbursementList2 = JSON.parse(xhr.responseText);
                appendReimbursementToDom(reimbursementList);
            }
        }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `account=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);


}

function getAllRejected(){
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "REJ-ALL"

        xhr.open("POST", "/project1/reimbursement");
        xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                let reimbursementList = JSON.parse(xhr.responseText);
                reimbursementList2 = JSON.parse(xhr.responseText);
                appendReimbursementToDom(reimbursementList);
            }
        }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `account=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);


}

function approveDeny(e){
    e.preventDefault();
    const xhr = new XMLHttpRequest();
    const token =  window.localStorage.getItem("token");
    let approveOrDeny = document.getElementsByName("AD");
    let reason = document.getElementById("reason").value;
    approveOrDeny = displayRadioValue(approveOrDeny);
    console.log(approveOrDeny);
    let checkboxArray = getCheckbox();
    checkboxArray = checkboxArray.toString();
    let checkboxString = checkboxArray.toString();
    const account = token.split(':');

    xhr.open("POST", "/project1/reimbursement");
     xhr.onreadystatechange = function(){
        if(xhr.readyState===4){
            window.location.reload()
        }
       }
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    const requestBody = `account=${account[0]}&type=${approveOrDeny}&reason=${reason}&checkBox=${checkboxString}`;
    xhr.send(requestBody);

}

function showEmployeeProfile(){

}
function showProfile(employeeList){
    reimbursementShow.hidden = true;
    employeeShow.hidden = false;
    document.getElementById('tprofile').innerHTML = '';

    let employeeTable = document.getElementById("tprofile");

    for(let employee of employeeList){
         let row = document.createElement("tr");
         row.className ="table-dark";
         let employeeId = document.createElement("td");
         employeeId.innerHTML = employee.employeeId;
         row.appendChild(employeeId);
         let employeeFName = document.createElement("td");
         employeeFName.innerHTML = employee.employeeFName;
         row.appendChild(employeeFName);
         let employeeLName = document.createElement("td");
         employeeLName.innerHTML = employee.employeeLName;
         row.appendChild(employeeLName);
         let employeeEmail= document.createElement("td");
         employeeEmail.innerHTML = employee.employeeEmail;
         row.appendChild(employeeEmail);
         employeeTable.appendChild(row);
     }

}

function appendReimbursementToDom(reimbursementList){
    document.getElementById('tbody').innerHTML = '';

    employeeShow.hidden = true;
    reimbursementShow.hidden = false;

    let reimbursementTable = document.getElementById("tbody");

    for (let reimbursement of reimbursementList) {
        let row = document.createElement("tr");
        row.className ="table-dark";
        let reimbursementId = document.createElement("td");
        reimbursementId.innerHTML = reimbursement.reimbursementId;
        row.appendChild(reimbursementId);
        let reimbursementAmount = document.createElement("td");
        reimbursementAmount.innerHTML = reimbursement.amount;
        row.appendChild(reimbursementAmount);
        let reimbursementInfo = document.createElement("td");
        reimbursementInfo.innerHTML = reimbursement.reimbursementInfo;
        row.appendChild(reimbursementInfo);
        let employee = document.createElement("td");
        employee.innerHTML = reimbursement.employee.employeeFName;
        row.appendChild(employee);
        let status = document.createElement("td");
        status.innerHTML = reimbursement.status;
        row.appendChild(status);
        let manager = document.createElement("td");
        manager.innerHTML =  reimbursement.manager.managerFName;
        row.appendChild(manager);
        let reason = document.createElement("td");
        reason.innerHTML = reimbursement.reason;
        row.appendChild(reason);
        checkbox = document.createElement('input');
        checkbox.type = "checkbox";
        checkbox.name = "reimbursementSelection";
        checkbox.class = "checkbox"
        checkbox.value = `${reimbursement.reimbursementId}`;
        checkbox.id =`id-${reimbursement.reimbursementId}`;
        row.appendChild(checkbox);
        reimbursementTable.appendChild(row);
    }

}

function getCheckbox(){
    let checkbox = document.getElementsByName("reimbursementSelection")
    let checkboxArray = new Array();
        for(let i = 0; i < checkbox.length; i++){
            if(checkbox[i].checked == true){
                checkboxArray.push(checkbox[i].value)
            }
        }
    return checkboxArray;
}
function displayRadioValue(approveDeny) {
    for(i = 0; i < approveDeny.length; i++) {
        if(approveDeny[i].checked)
            return approveDeny[i].value;
    }
}
