document.getElementById("login-btn").addEventListener("click", login);

function login(){
    const errorDiv = document.getElementById("error-div");
    errorDiv.hidden = true;

    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
    let account = document.getElementsByName("account");
    account = displayRadioValue(account);


    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/project1/login");

    xhr.onreadystatechange = function(){
        if(xhr.readyState===4){

         if(xhr.status === 401){
                errorDiv.hidden = false;
                errorDiv.innerText = "invalid credentials";
         }else if(xhr.status==200){
         const token = xhr.getResponseHeader("Authorization");
         const account = token.split(':');
         localStorage.setItem("token", token);

         if(account[1] == "EMPLOYEE"){
            window.location.href="/project1/Employee.html";
         }else{
            window.location.href="/project1/Manager.html";
         }
        }
        }
    }
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    const requestBody = `email=${email}&password=${password}&account=${account}`;
    xhr.send(requestBody);
}

function displayRadioValue(account) {
    for(i = 0; i < account.length; i++) {
        if(account[i].checked)
            return account[i].value;
    }
}
