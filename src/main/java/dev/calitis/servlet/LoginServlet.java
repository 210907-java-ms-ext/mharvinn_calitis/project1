package dev.calitis.servlet;

import dev.calitis.model.Employee;
import dev.calitis.model.Manager;
import dev.calitis.service.EmployeeServiceImpl;
import dev.calitis.service.ManagerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    private int test;
    private EmployeeServiceImpl employeeService = new EmployeeServiceImpl();
    private Employee employee = new Employee();
    private ManagerServiceImpl managerService = new ManagerServiceImpl();
    private Manager manager = new Manager();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String email = req.getParameter("email");
            String password = req.getParameter("password");
            String account = req.getParameter("account");

            if (account.equals("EMPLOYEE")) {

                employee = employeeService.login(email, password);

                if (employee == null) {
                    resp.sendError(401, "User credentials provided did not return a valid account");
                } else {
                    resp.setStatus(200);
                    String token = employee.getEmployeeId() + ":" + "EMPLOYEE";
                    resp.setHeader("Authorization", token);
                }
            } else {
                manager = managerService.login(email, password);

                if (manager == null) {
                    resp.sendError(401, "User credentials provided did not return a valid account");
                } else {
                    resp.setStatus(200);
                    String token = manager.getManager_id() + ":" + "MANAGER";
                    resp.setHeader("Authorization", token);
                }
            }
    }

}
