package dev.calitis.servlet;

import dev.calitis.model.Employee;
import dev.calitis.service.EmployeeServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;

public class EmployeeServlet extends HttpServlet {

    private EmployeeServiceImpl  employeeService = new EmployeeServiceImpl();
    private Employee employee = new Employee();

    @Override
    protected  void doPost(HttpServletRequest req, HttpServletResponse resp)throws IOException, ServletException{

        String type = req.getParameter("type");
        String user_id = req.getParameter("employee_id");
        int user;
        user = Integer.parseInt(user_id);

        //Check to see what employee function to call
        if(type.equals("EMP-ID")){
            employee = employeeService.getEmployeeById(user);

            ObjectMapper objectMapper = new ObjectMapper();

            String json = objectMapper.writeValueAsString(employee);

            resp.getWriter().write(json);

            resp.setStatus(200);
        }else if(type.equals("EMP-ALL")){
            List<Employee> employeeList = employeeService.getAllEmployees();
            ObjectMapper objectMapper = new ObjectMapper();

            String json = objectMapper.writeValueAsString(employeeList);

            System.out.println(json);

            resp.getWriter().write(json);

            resp.setStatus(200);
        }


    }

}
