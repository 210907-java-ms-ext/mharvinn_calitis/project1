package dev.calitis.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.calitis.model.Reimbursement;
import dev.calitis.service.ReimbursementService;
import dev.calitis.service.ReimbursementServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.internal.util.logging.Log;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReimbursementServlet extends HttpServlet {

    private static final Logger LOG = LogManager.getLogger(Reimbursement.class);

    ReimbursementServiceImpl reimbursementService = new ReimbursementServiceImpl();
    Reimbursement reimbursement = new Reimbursement();

    @Override
    protected  void doPost(HttpServletRequest req, HttpServletResponse resp)throws IOException, ServletException {
        String type = req.getParameter("type");
        String reason = "";
        List<Integer> checkNum = new ArrayList<>();
        int user = 0;

        //Check statement that checks to see which function it should call.
        if(type.equals("ALL")||type.equals("APP-ALL")||type.equals("PEN-ALL")||type.equals("REJ-ALL")){}
        else if(type.equals("APPROVE")||type.equals("DENY")){
            String checkbox= req.getParameter("checkBox");
            String[] arrCheckbox = checkbox.split(",");
            for(String s: arrCheckbox){
                checkNum.add(Integer.parseInt(s));
            }
            reason = req.getParameter("reason");
            String user_id = req.getParameter("account");
            user = Integer.parseInt(user_id);
        }else {
            String user_id = req.getParameter("employee_id");
            user = Integer.parseInt(user_id);
        }

        //If else if to check which reimbursement service to call.
        if (type.equals("ID")) {
            List<Reimbursement> reimbursements = reimbursementService.getAllReimbursementsById(user);
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(reimbursements);

            resp.getWriter().write(json);
            resp.setStatus(200);
        } else if (type.equals("ID-PEN")) {
            List<Reimbursement> reimbursements = reimbursementService.getPendingReimbursements(user);
            ObjectMapper objectMapper = new ObjectMapper();

            String json = objectMapper.writeValueAsString(reimbursements);
            LOG.debug("Completed");

            resp.getWriter().write(json);
            resp.setStatus(200);
        } else if (type.equals("ID-APP")) {
            List<Reimbursement> reimbursements = reimbursementService.getApprovedReimbursementsById(user);
            ObjectMapper objectMapper = new ObjectMapper();

            String json = objectMapper.writeValueAsString(reimbursements);

            resp.getWriter().write(json);
            resp.setStatus(200);
        } else if (type.equals("ID-REJ")) {
            List<Reimbursement> reimbursements = reimbursementService.getRejectedReimbursementsById(user);
            ObjectMapper objectMapper = new ObjectMapper();

            String json = objectMapper.writeValueAsString(reimbursements);

            resp.getWriter().write(json);
            resp.setStatus(200);

        } else if (type.equals("ID-CREATE")) {
            String info = req.getParameter("description");
            BigDecimal amt = new BigDecimal(req.getParameter("amount"));
            reimbursementService.createNewReimbursement(user, amt, info);
            resp.setStatus(200);
        }else if(type.equals("ALL")){
            List<Reimbursement> reimbursements = reimbursementService.getAllReimbursements();
            ObjectMapper objectMapper = new ObjectMapper();

            String json = objectMapper.writeValueAsString(reimbursements);

            resp.getWriter().write(json);
            resp.setStatus(200);
        }else if(type.equals("APP-ALL")){
            List<Reimbursement> reimbursements = reimbursementService.getApprovedReimbursements();
            ObjectMapper objectMapper = new ObjectMapper();

            String json = objectMapper.writeValueAsString(reimbursements);

            resp.getWriter().write(json);
            resp.setStatus(200);
        }else if(type.equals("REJ-ALL")){
            List<Reimbursement> reimbursements = reimbursementService.getRejectedReimbursements();
            ObjectMapper objectMapper = new ObjectMapper();

            String json = objectMapper.writeValueAsString(reimbursements);

            resp.getWriter().write(json);
            resp.setStatus(200);
        }else if(type.equals("PEN-ALL")){
            List<Reimbursement> reimbursements = reimbursementService.getAllPendingReimbursements();
            ObjectMapper objectMapper = new ObjectMapper();

            String json = objectMapper.writeValueAsString(reimbursements);

            resp.getWriter().write(json);
            resp.setStatus(200);
        } else if(type.equals("APPROVE")){
            reimbursementService.approveReimbursement(checkNum,user,reason);
            resp.setStatus(200);
        }else if(type.equals("DENY")){
            reimbursementService.denyReimbursement(checkNum,user,reason);
            resp.setStatus(200);
        }
    }
}
