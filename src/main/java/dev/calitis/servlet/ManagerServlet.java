package dev.calitis.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.calitis.model.Manager;
import dev.calitis.service.ManagerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ManagerServlet extends HttpServlet {

    private ManagerServiceImpl managerService = new ManagerServiceImpl();
    private Manager manager = new Manager();

    @Override
    protected  void doGet(HttpServletRequest req, HttpServletResponse resp)throws IOException, ServletException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        manager = managerService.login(email, password);

        ObjectMapper objectMapper = new ObjectMapper();

        String json = objectMapper.writeValueAsString(manager);

        resp.getWriter().write(json);

        resp.setStatus(200);
    }

}
