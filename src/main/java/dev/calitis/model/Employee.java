package dev.calitis.model;

import java.util.Objects;

public class Employee {

    private Manager manager;

    private String employeeEmail;

    private String employeePassword;

    private String employeeFName;

    private String employeeLName;

    private int employeeId;

    public Employee() {
       super();
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeePassword() {
        return employeePassword;
    }

    public void setEmployeePassword(String employeePassword) {
        this.employeePassword = employeePassword;
    }


    public String getEmployeeFName() {
        return employeeFName;
    }

    public void setEmployeeFName(String employeeFName) {
        this.employeeFName = employeeFName;
    }
    public String getEmployeeLName() {
        return employeeLName;
    }

    public void setEmployeeLName(String employeeLName) {
        this.employeeLName = employeeLName;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return employeeId == employee.employeeId && manager.equals(employee.manager) && employeeEmail.equals(employee.employeeEmail) && employeePassword.equals(employee.employeePassword) && employeeFName.equals(employee.employeeFName) && employeeLName.equals(employee.employeeLName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(manager, employeeEmail, employeePassword, employeeFName, employeeLName, employeeId);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "manager=" + manager +
                ", employeeEmail='" + employeeEmail + '\'' +
                ", employeePassword='" + employeePassword + '\'' +
                ", employeeFName='" + employeeFName + '\'' +
                ", employeeLName='" + employeeLName + '\'' +
                ", employee_id=" + employeeId +
                '}';
    }
}

