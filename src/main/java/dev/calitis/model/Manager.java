package dev.calitis.model;

import java.util.Objects;

public class Manager {

    private int manager_id;
    private String managerEmail;
    private String managerPassword;
    private String managerFName;
    private String managerLName;

    public Manager() {
        super();
    }

    public int getManager_id() {
        return manager_id;
    }

    public void setManager_id(int manager_id) {
        this.manager_id = manager_id;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getManagerPassword() {
        return managerPassword;
    }

    public void setManagerPassword(String managerPassword) {
        this.managerPassword = managerPassword;
    }

    public String getManagerFName() {
        return managerFName;
    }

    public void setManagerFName(String managerFName) {
        this.managerFName = managerFName;
    }



    public String getManangerLName() {
        return managerLName;
    }

    public void setManangerLName(String manangerLName) {
        this.managerLName = manangerLName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Manager manager = (Manager) o;
        return manager_id == manager.manager_id && managerEmail.equals(manager.managerEmail) && managerPassword.equals(manager.managerPassword) && managerFName.equals(manager.managerFName) && managerLName.equals(manager.managerLName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(manager_id, managerEmail, managerPassword, managerFName, managerLName);
    }

    @Override
    public String toString() {
        return "Manager{" +
                "manager_id=" + manager_id +
                ", managerEmail='" + managerEmail + '\'' +
                ", managerPassword='" + managerPassword + '\'' +
                ", managerFName='" + managerFName + '\'' +
                ", managerLName='" + managerLName + '\'' +
                '}';
    }
}
