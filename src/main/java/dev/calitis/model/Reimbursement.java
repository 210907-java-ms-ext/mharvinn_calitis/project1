package dev.calitis.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Reimbursement {
    private int reimbursementId;
    private String reimbursementInfo;
    private String reason;
    private BigDecimal amount;
    private boolean isPending;
    private boolean isApproved;
    private boolean isRejected;
    private String status;
    private Employee employee;
    private Manager manager;



    public Reimbursement() {
        super();
    }

    public int getReimbursementId() {
        return reimbursementId;
    }

    public void setReimbursementId(int reimbursementId) {
        this.reimbursementId = reimbursementId;
    }

    public String getReimbursementInfo() {
        return reimbursementInfo;
    }

    public void setReimbursementInfo(String reimbursementInfo) {
        this.reimbursementInfo = reimbursementInfo;
    }


    public boolean isPending(){
        return isPending;
    }
    public void setPending(boolean pending){
        isPending = pending;
    }
    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }
    public boolean isRejected() {
        return isRejected;
    }

    public void setRejected(boolean rejected) {
        isRejected = rejected;
    }


    public Manager getManager() {return manager;}

    //Populates manager for null values for display with table.
    public void setManager(Manager manager) {
        if(manager.getManagerFName() == null){
            this.manager = new Manager();
            this.manager.setManagerFName("NA");
            this.manager.setManangerLName("NA");
            System.out.println(this.manager);
        }else{
            this.manager = manager;
        }
    }
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public String getStatus() {
        return status;
    }
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        if(reason == null){
            this.reason = "Awaiting Manager Review";
        }else {
            this.reason = reason;
        }
    }

    //Changes booleans into an easy to read status for the table.
    public void setStatus() {
        if(isRejected == true || isApproved == true){
            if(isRejected == true){
                this.status = "Rejected";
            }else if(isApproved == true){
                this.status = "Approved";
            }
        }else{
            this.status = "Pending";
        }
    }



    @Override
    public String toString() {
        return "Reimbursement{" +
                "reimbursementId=" + reimbursementId +
                ", reimbursementInfo='" + reimbursementInfo + '\'' +
                ", isPending=" + isPending +
                ", isApproved=" + isApproved +
                ", isRejected=" + isRejected +
                ", status='" + status + '\'' +
                ", employee=" + employee +
                ", manager=" + manager +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reimbursement that = (Reimbursement) o;
        return reimbursementId == that.reimbursementId && isPending == that.isPending && isApproved == that.isApproved && isRejected == that.isRejected && Objects.equals(reimbursementInfo, that.reimbursementInfo) && Objects.equals(amount, that.amount) && Objects.equals(status, that.status) && Objects.equals(employee, that.employee) && Objects.equals(manager, that.manager);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reimbursementId, reimbursementInfo, amount, isPending, isApproved, isRejected, status, employee, manager);
    }

}
