package dev.calitis.data;

import dev.calitis.model.Manager;

public interface ManagerDao {

    public Manager login(String email, String password);
}
