package dev.calitis.data;

import dev.calitis.model.Employee;
import dev.calitis.service.ConnectionService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoImpl implements EmployeeDao {

    private ConnectionService connectionService;

    public Employee login(String email, String password){
        String sql = "select * from employee where employee_email = ? and employee_password = ?";
        Employee employee = null;

        connectionService = new ConnectionService();

        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, email);
            pstmt.setString(2,password);

            ResultSet resultSet = pstmt.executeQuery();

            while(resultSet.next()){
                employee = new Employee();
                employee.setEmployeeId(resultSet.getInt("employee_id"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                employee.setEmployeeEmail(resultSet.getString("employee_email"));
                employee.setEmployeePassword(resultSet.getString("employee_password"));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

        return employee;

    }

    @Override
    public Employee getProfile(int id) {
        String sql = "select * from employee where employee_id=?";
        Employee employee = null;

        connectionService = new ConnectionService();

        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, id);
            ResultSet resultSet = pstmt.executeQuery();

            while(resultSet.next()){
                employee = new Employee();
                employee.setEmployeeId(resultSet.getInt("employee_id"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                employee.setEmployeeEmail(resultSet.getString("employee_email"));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

        return employee;
    }

    @Override
    public List<Employee> getAllEmployee(){
        String sql = "select * from employee";
        Employee employee = null;

        connectionService = new ConnectionService();

        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);
            ResultSet resultSet = pstmt.executeQuery();
            List<Employee> employeeList = new ArrayList<>();

            while(resultSet.next()){
                employee = new Employee();
                employee.setEmployeeId(resultSet.getInt("employee_id"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                employee.setEmployeeEmail(resultSet.getString("employee_email"));
                employeeList.add(employee);
            }
            return employeeList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

    }

}
