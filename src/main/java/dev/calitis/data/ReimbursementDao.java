package dev.calitis.data;

import dev.calitis.model.Reimbursement;

import java.math.BigDecimal;
import java.util.List;

public interface ReimbursementDao {

    public List<Reimbursement> getReimbursmentByEmployeeID(int id);
    public List<Reimbursement> getAllReimbursement();
    public List<Reimbursement> getAllPendingReimbursement();
    public List<Reimbursement> getAllPendingReimbursementById(int id);
    public List<Reimbursement> getAllApprovedReimbursementById(int id);
    public List<Reimbursement> getAllRejectedReimbursementById(int id);
    public List<Reimbursement> getAllApprovedReimbursement();
    public List<Reimbursement> getAllDeniedReimbursement();
    public void createNewReimbursement(int id, BigDecimal amt, String info);
    public void approveReimbursement(List<Integer> checkbox, int id, String info);
    public void denyReimbursement(List<Integer> checkbox, int id, String info);
}
