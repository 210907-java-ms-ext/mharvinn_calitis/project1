package dev.calitis.data;

import dev.calitis.model.Employee;

import java.util.List;

public interface EmployeeDao {

    public Employee login(String email, String password);
    public Employee getProfile(int id);
    public List<Employee> getAllEmployee();

}
