package dev.calitis.data;

import dev.calitis.model.Employee;
import dev.calitis.model.Manager;
import dev.calitis.model.Reimbursement;
import dev.calitis.service.ConnectionService;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementDaoImpl implements ReimbursementDao{

    private ConnectionService connectionService;

    @Override
    public void createNewReimbursement(int id, BigDecimal amt, String info) {
        String sql = "insert into reimbursement (amount, reimbursement_info, ispending, isapproved, isrejected, employee_id) values(?,?,true,false,false, ?)";

        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setBigDecimal(1, amt);
            pstmt.setString(2, info);
            pstmt.setInt(3, id);
            pstmt.executeUpdate();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //Approve reinbursement loops through depending on how many boxes are checked.
    @Override
    public void approveReimbursement(List<Integer> checkbox, int id,String info) {
        String sql = "update reimbursement " +
                "set ispending = false, isapproved = true , isrejected = false, manager_id = ?, reason = ? "+
                "where (reimbursement_id) in (";
        for(int i = 0; i<=(checkbox.size()-1);i++){
            if(i == (checkbox.size()-1)){
                sql += "?)";
            }else {
                sql += "?,";
            }
        }

        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1,id);
            pstmt.setString(2,info);
            for (int i = 1; i <= checkbox.size(); i++) {
                pstmt.setInt((i+2),checkbox.get(i-1));
            }
            pstmt.executeUpdate();
        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //Deny reimbursement it loops through depending on how much is checked.
    @Override
    public void denyReimbursement(List<Integer> checkbox, int id, String info) {
        String sql = "update reimbursement " +
                "set ispending = false, isapproved = false , isrejected = true, manager_id = ?, reason = ? "+
                "where (reimbursement_id) in (";
        for(int i = 0; i<=(checkbox.size()-1);i++){
            if(i == (checkbox.size()-1)){
                sql += "?)";
            }else {
                sql += "?,";
            }
        }

        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1,id);
            pstmt.setString(2,info);
            for (int i = 1; i <= checkbox.size(); i++) {
                pstmt.setInt((i+2),checkbox.get(i-1));
            }
            pstmt.executeUpdate();
        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //All the reimbursement methods based on getting ID
    @Override
    public List<Reimbursement> getAllPendingReimbursementById(int id) {
        Employee employee = new Employee();
        employee.setEmployeeId(id);
        String sql = "select reimbursement.reimbursement_id, " +
                "reimbursement.reimbursement_info, " +
                "reimbursement.ispending, " +
                "reimbursement.isapproved, " +
                "reimbursement.isrejected, "+
                "reimbursement.amount,"+
                "employee.employee_fname, " +
                "employee.employee_lname " +
                "from reimbursement inner join employee on reimbursement.employee_id = employee.employee_id "+
                "where employee.employee_id = ? and reimbursement.ispending = true";

        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1,id);

            ResultSet resultSet = pstmt.executeQuery();

            List<Reimbursement> reimbursementList = new ArrayList<>();

            while(resultSet.next()){
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setReimbursementId(resultSet.getInt("reimbursement_id"));
                reimbursement.setReimbursementInfo(resultSet.getString("reimbursement_info"));
                reimbursement.setApproved(resultSet.getBoolean("isapproved"));
                reimbursement.setPending(resultSet.getBoolean("ispending"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                reimbursement.setAmount(resultSet.getBigDecimal("amount"));
                reimbursement.setRejected(resultSet.getBoolean("isrejected"));
                reimbursement.setStatus();
                reimbursement.setEmployee(employee);
                reimbursementList.add(reimbursement);

            }
            return reimbursementList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Reimbursement> getAllApprovedReimbursementById(int id) {
        Employee employee = new Employee();
        employee.setEmployeeId(id);
        String sql = "select reimbursement.reimbursement_id, " +
                "reimbursement.reimbursement_info, " +
                "reimbursement.ispending, " +
                "reimbursement.isapproved, "+
                "reimbursement.isrejected, "+
                "reimbursement.amount,"+
                "employee.employee_fname, " +
                "employee.employee_lname " +
                "from reimbursement inner join employee on reimbursement.employee_id = employee.employee_id "+
                "where employee.employee_id = ? and reimbursement.isapproved = true";

        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1,id);

            ResultSet resultSet = pstmt.executeQuery();

            List<Reimbursement> reimbursementList = new ArrayList<>();

            while(resultSet.next()){
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setReimbursementId(resultSet.getInt("reimbursement_id"));
                reimbursement.setReimbursementInfo(resultSet.getString("reimbursement_info"));
                reimbursement.setApproved(resultSet.getBoolean("isapproved"));
                reimbursement.setPending(resultSet.getBoolean("ispending"));
                reimbursement.setRejected(resultSet.getBoolean("isrejected"));
                reimbursement.setAmount(resultSet.getBigDecimal("amount"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                reimbursement.setStatus();
                reimbursement.setEmployee(employee);
                reimbursementList.add(reimbursement);

            }
            return reimbursementList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Reimbursement> getAllRejectedReimbursementById(int id) {
        Employee employee = new Employee();
        employee.setEmployeeId(id);
        String sql = "select reimbursement.reimbursement_id, " +
                "reimbursement.reimbursement_info, " +
                "reimbursement.ispending, " +
                "reimbursement.isapproved, "+
                "reimbursement.isrejected, "+
                "reimbursement.amount,"+
                "employee.employee_fname, " +
                "employee.employee_lname " +
                "from reimbursement inner join employee on reimbursement.employee_id = employee.employee_id "+
                "where employee.employee_id = ? and reimbursement.isrejected = true";

        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1,id);

            ResultSet resultSet = pstmt.executeQuery();

            List<Reimbursement> reimbursementList = new ArrayList<>();

            while(resultSet.next()){
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setReimbursementId(resultSet.getInt("reimbursement_id"));
                reimbursement.setReimbursementInfo(resultSet.getString("reimbursement_info"));
                reimbursement.setApproved(resultSet.getBoolean("isapproved"));
                reimbursement.setPending(resultSet.getBoolean("ispending"));
                reimbursement.setRejected(resultSet.getBoolean("isrejected"));
                reimbursement.setAmount(resultSet.getBigDecimal("amount"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                reimbursement.setStatus();
                reimbursement.setEmployee(employee);
                reimbursementList.add(reimbursement);

            }
            return reimbursementList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }


    @Override
    public List<Reimbursement> getReimbursmentByEmployeeID(int id) {
        Employee employee = new Employee();
        employee.setEmployeeId(id);
        String sql = "select reimbursement.reimbursement_id, " +
                "reimbursement.reimbursement_info, " +
                "reimbursement.ispending, " +
                "reimbursement.isapproved, " +
                "reimbursement.isrejected, "+
                "reimbursement.amount, "+
                "employee.employee_fname, " +
                "employee.employee_lname " +
                "from reimbursement inner join employee on reimbursement.employee_id = employee.employee_id "+
                "where employee.employee_id = ?";

        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1,id);

            ResultSet resultSet = pstmt.executeQuery();

            List<Reimbursement> reimbursementList = new ArrayList<>();

            while(resultSet.next()){
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setReimbursementId(resultSet.getInt("reimbursement_id"));
                reimbursement.setReimbursementInfo(resultSet.getString("reimbursement_info"));
                reimbursement.setApproved(resultSet.getBoolean("isapproved"));
                reimbursement.setPending(resultSet.getBoolean("ispending"));
                reimbursement.setAmount(resultSet.getBigDecimal("amount"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                reimbursement.setRejected(resultSet.getBoolean("isrejected"));
                reimbursement.setStatus();
                reimbursement.setEmployee(employee);
                reimbursementList.add(reimbursement);

            }
            return reimbursementList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }


    //The all reimbursement display methods for managers.
    @Override
    public List<Reimbursement> getAllApprovedReimbursement() {
        Employee employee = new Employee();
        Manager manager = new Manager();
        String sql = "select reimbursement.reimbursement_id," +
                "reimbursement.reimbursement_info," +
                "reimbursement.amount,"+
                "reimbursement.ispending," +
                "reimbursement.isapproved,"+
                "reimbursement.amount,"+
                "reimbursement.isrejected,"+
                "employee.employee_fname," +
                "employee.employee_lname," +
                "manager.manager_fname," +
                "manager.manager_lname," +
                "reimbursement.reason " +
                "from reimbursement inner join employee on reimbursement.employee_id = employee.employee_id " +
                "left join manager on reimbursement.manager_id = manager.manager_id "+
                "where isapproved = true;";
        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);

            ResultSet resultSet = pstmt.executeQuery();

            List<Reimbursement> reimbursementList = new ArrayList<>();

            while(resultSet.next()){
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setReimbursementId(resultSet.getInt("reimbursement_id"));
                reimbursement.setReimbursementInfo(resultSet.getString("reimbursement_info"));
                reimbursement.setApproved(resultSet.getBoolean("isapproved"));
                reimbursement.setPending(resultSet.getBoolean("ispending"));
                reimbursement.setAmount(resultSet.getBigDecimal("amount"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                reimbursement.setReason(resultSet.getString("reason"));
                reimbursement.setRejected(resultSet.getBoolean("isrejected"));
                reimbursement.setStatus();
                reimbursement.setEmployee(employee);
                manager.setManagerFName(resultSet.getString("manager_fname"));
                manager.setManangerLName(resultSet.getString("manager_lname"));
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);

            }
            return reimbursementList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Reimbursement> getAllDeniedReimbursement() {
        Employee employee = new Employee();
        Manager manager = new Manager();
        String sql = "select reimbursement.reimbursement_id," +
                "reimbursement.reimbursement_info," +
                "reimbursement.amount,"+
                "reimbursement.ispending," +
                "reimbursement.isapproved,"+
                "reimbursement.isrejected,"+
                "employee.employee_fname," +
                "employee.employee_lname," +
                "manager.manager_fname," +
                "manager.manager_lname," +
                "reimbursement.reason " +
                "from reimbursement inner join employee on reimbursement.employee_id = employee.employee_id " +
                "left join manager on reimbursement.manager_id = manager.manager_id "+
                "where isrejected = true;";
        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);

            ResultSet resultSet = pstmt.executeQuery();

            List<Reimbursement> reimbursementList = new ArrayList<>();

            while(resultSet.next()){
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setReimbursementId(resultSet.getInt("reimbursement_id"));
                reimbursement.setReimbursementInfo(resultSet.getString("reimbursement_info"));
                reimbursement.setApproved(resultSet.getBoolean("isapproved"));
                reimbursement.setPending(resultSet.getBoolean("ispending"));
                reimbursement.setAmount(resultSet.getBigDecimal("amount"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                reimbursement.setReason(resultSet.getString("reason"));
                reimbursement.setRejected(resultSet.getBoolean("isrejected"));
                reimbursement.setStatus();
                reimbursement.setEmployee(employee);
                manager.setManagerFName(resultSet.getString("manager_fname"));
                manager.setManangerLName(resultSet.getString("manager_lname"));
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);

            }
            return reimbursementList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Reimbursement> getAllPendingReimbursement() {
        Employee employee = new Employee();
        Manager manager = new Manager();
        String sql = "select reimbursement.reimbursement_id," +
                "reimbursement.reimbursement_info," +
                "reimbursement.ispending," +
                "reimbursement.isapproved,"+
                "reimbursement.isrejected,"+
                "reimbursement.amount,"+
                "employee.employee_fname," +
                "employee.employee_lname," +
                "manager.manager_fname," +
                "manager.manager_lname," +
                "reimbursement.reason " +
                "from reimbursement inner join employee on reimbursement.employee_id = employee.employee_id " +
                "left join manager on reimbursement.manager_id = manager.manager_id "+
                "where ispending = true;";
        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);

            ResultSet resultSet = pstmt.executeQuery();

            List<Reimbursement> reimbursementList = new ArrayList<>();

            while(resultSet.next()){
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setReimbursementId(resultSet.getInt("reimbursement_id"));
                reimbursement.setReimbursementInfo(resultSet.getString("reimbursement_info"));
                reimbursement.setApproved(resultSet.getBoolean("isapproved"));
                reimbursement.setPending(resultSet.getBoolean("ispending"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                reimbursement.setReason(resultSet.getString("reason"));
                reimbursement.setRejected(resultSet.getBoolean("isrejected"));
                reimbursement.setAmount(resultSet.getBigDecimal("amount"));
                reimbursement.setStatus();
                reimbursement.setEmployee(employee);
                manager.setManagerFName(resultSet.getString("manager_fname"));
                manager.setManangerLName(resultSet.getString("manager_lname"));
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);

            }
            return reimbursementList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }


    @Override
    public List<Reimbursement> getAllReimbursement() {
        Employee employee = new Employee();
        Manager manager = new Manager();
        String sql = "select reimbursement.reimbursement_id," +
                "reimbursement.reimbursement_info," +
                "reimbursement.ispending," +
                "reimbursement.isapproved,"+
                "reimbursement.isrejected,"+
                "reimbursement.amount,"+
                "employee.employee_fname," +
                "employee.employee_lname," +
                "manager.manager_fname," +
                "manager.manager_lname," +
                "reimbursement.reason " +
                "from reimbursement inner join employee on reimbursement.employee_id = employee.employee_id " +
                "left join manager on reimbursement.manager_id = manager.manager_id ;";
        connectionService = new ConnectionService();
        try(Connection connection = connectionService.getConnection()){
            PreparedStatement pstmt = connection.prepareStatement(sql);

            ResultSet resultSet = pstmt.executeQuery();

            List<Reimbursement> reimbursementList = new ArrayList<>();

            while(resultSet.next()){
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setReimbursementId(resultSet.getInt("reimbursement_id"));
                reimbursement.setReimbursementInfo(resultSet.getString("reimbursement_info"));
                reimbursement.setApproved(resultSet.getBoolean("isapproved"));
                reimbursement.setPending(resultSet.getBoolean("ispending"));
                employee.setEmployeeFName(resultSet.getString("employee_fname"));
                employee.setEmployeeLName(resultSet.getString("employee_lname"));
                reimbursement.setReason(resultSet.getString("reason"));
                reimbursement.setRejected(resultSet.getBoolean("isrejected"));
                reimbursement.setAmount((resultSet.getBigDecimal("amount")));
                reimbursement.setStatus();
                reimbursement.setEmployee(employee);
                manager.setManagerFName(resultSet.getString("manager_fname"));
                manager.setManangerLName(resultSet.getString("manager_lname"));
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);

            }
            return reimbursementList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

}
