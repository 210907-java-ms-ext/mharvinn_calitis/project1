package dev.calitis.data;

import dev.calitis.model.Employee;
import dev.calitis.model.Manager;
import dev.calitis.service.ConnectionService;
import dev.calitis.service.ManagerService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ManagerDaoImpl implements  ManagerDao {
    private ConnectionService connectionService;

    public Manager login(String email, String password) {
        String sql = "select * from manager where manager_email = ? and manager_password = ?";

        connectionService = new ConnectionService();

        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, email);
            pstmt.setString(2, password);

            ResultSet resultSet = pstmt.executeQuery();

            Manager manager = new Manager();

            while (resultSet.next()) {
                manager.setManager_id(resultSet.getInt("manager_id"));
                manager.setManagerFName(resultSet.getString("manager_fname"));
                manager.setManangerLName(resultSet.getString("manager_lname"));
                manager.setManagerEmail(resultSet.getString("manager_email"));
            }

            return manager;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (NullPointerException throwables){
            throwables.printStackTrace();
        }

        return null;
    }
}
