package dev.calitis.service;

import dev.calitis.data.EmployeeDaoImpl;
import dev.calitis.model.Employee;

import java.util.List;

public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDaoImpl employee = new EmployeeDaoImpl();

    @Override
    public Employee login(String email, String password){
        return employee.login(email,password);
    }

    @Override
    public Employee getEmployeeById(int id) {
        return employee.getProfile(id);
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employee.getAllEmployee();
    }
}
