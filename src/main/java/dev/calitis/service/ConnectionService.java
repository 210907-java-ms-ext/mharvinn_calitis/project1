package dev.calitis.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionService {

    private Connection connection;

    public ConnectionService() {
        super();

        try {
            Class.forName("org.postgresql.Driver");

            String url = System.getenv("url");
            String username = System.getenv("postgres_user");
            String password = System.getenv("postgres_pass");
            connection = DriverManager.getConnection(url, username, password);
            connection.setSchema("Project1");

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public Connection getConnection() {
        System.out.println(connection);
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

}
