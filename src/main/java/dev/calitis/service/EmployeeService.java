package dev.calitis.service;

import dev.calitis.model.Employee;

import java.util.List;

public interface EmployeeService {
    public Employee login(String email, String password);
    public Employee getEmployeeById(int id);
    public List<Employee>  getAllEmployees();
}
