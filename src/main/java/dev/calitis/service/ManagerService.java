package dev.calitis.service;

import dev.calitis.model.Manager;

public interface ManagerService {
    public Manager login(String email,String password);
}
