package dev.calitis.service;

import dev.calitis.data.ManagerDao;
import dev.calitis.data.ManagerDaoImpl;
import dev.calitis.model.Manager;

public class ManagerServiceImpl implements ManagerService {
    private ManagerDaoImpl manager = new ManagerDaoImpl();

    @Override
    public Manager login(String email, String password) {
        return manager.login(email,password);
    }
}
