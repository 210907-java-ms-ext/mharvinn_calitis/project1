package dev.calitis.service;

import dev.calitis.data.ReimbursementDaoImpl;
import dev.calitis.model.Reimbursement;

import java.math.BigDecimal;
import java.util.List;

public class ReimbursementServiceImpl implements ReimbursementService{
    ReimbursementDaoImpl reimbursementDao = new ReimbursementDaoImpl();
    @Override
    public List<Reimbursement> getPendingReimbursements(int id) {
        return  reimbursementDao.getAllPendingReimbursementById(id);
    }

    @Override
    public List<Reimbursement> getAllReimbursementsById(int id) {
        return reimbursementDao.getReimbursmentByEmployeeID(id);

    }

    @Override
    public List<Reimbursement> getAllPendingReimbursements() {

        return reimbursementDao.getAllPendingReimbursement();
    }


    @Override
    public List<Reimbursement> getApprovedReimbursementsById(int id) {
        return reimbursementDao.getAllApprovedReimbursementById(id);
    }

    @Override
    public List<Reimbursement> getApprovedReimbursements() {

        return reimbursementDao.getAllApprovedReimbursement();
    }

    @Override
    public List<Reimbursement> getRejectedReimbursements() {
        return reimbursementDao.getAllDeniedReimbursement();
    }

    @Override
    public void approveReimbursement(List<Integer> checked, int id, String info) {
        reimbursementDao.approveReimbursement(checked, id, info);
    }

    @Override
    public void denyReimbursement(List<Integer> checked, int id, String info) {
        reimbursementDao.denyReimbursement(checked, id, info);
    }

    @Override
    public List<Reimbursement> getRejectedReimbursementsById(int id) {
        return reimbursementDao.getAllRejectedReimbursementById(id);
    }

    @Override
    public List<Reimbursement> getAllReimbursements() {
        return reimbursementDao.getAllReimbursement();
    }

    @Override
    public void createNewReimbursement(int id, BigDecimal amount, String info) {
        reimbursementDao.createNewReimbursement(id, amount, info);
    }
}
