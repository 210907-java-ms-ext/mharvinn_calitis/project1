package dev.calitis.service;

import dev.calitis.model.Reimbursement;

import java.math.BigDecimal;
import java.sql.Array;
import java.util.List;

public interface ReimbursementService {

    public List<Reimbursement> getPendingReimbursements(int id);
    public List<Reimbursement> getAllReimbursementsById(int id);
    public List<Reimbursement> getApprovedReimbursementsById(int id);
    public List<Reimbursement> getRejectedReimbursementsById(int id);
    public List<Reimbursement> getAllReimbursements();
    public List<Reimbursement> getAllPendingReimbursements();
    public List<Reimbursement> getApprovedReimbursements();
    public List<Reimbursement> getRejectedReimbursements();
    public void approveReimbursement(List<Integer> checked, int id, String info);
    public void denyReimbursement(List<Integer> checked, int id, String info);
    public void createNewReimbursement(int id, BigDecimal amount, String info);
}
