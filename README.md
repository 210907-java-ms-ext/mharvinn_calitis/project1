# Project1
Expense Reimbursement System (ERS)

Project 1 is an employee reimbursement system in which a manager can manage employee reimbursement requests and choose to either accept or deny them.


# User Stories
---

- An Employee can login
- An Employee can view the Employee Homepage
- An Employee can logout
- An Employee can submit a reimbursement request
- An Employee can view their pending reimbursement requests
- An Employee can view their resolved reimbursement requests
- An Employee can view their information (profile)

---

- A Manager can login
- A Manager can view the Manager Homepage
- A Manager can logout
- A Manager can approve/deny pending reimbursement requests
- A Manager can view all pending requests from all employees
- A Manager can view all resolved requests from all employees and see which manager resolved it
- A Manager can view all Employees

---

- Some testing
- Logging using log4j 


---

Technologies:

- Java 1.8
- Servlets
- JDBC
- SQL
- HTML
- CSS
- Javascript
- Bootstrap (Optional)
- AJAX
- JUnit
- log4j 



