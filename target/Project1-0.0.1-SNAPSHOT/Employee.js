document.getElementById("LogOut").addEventListener("click",logout);
document.getElementById("Show-Employee-Reimbursement").addEventListener("click",showEmployeeReimbursement);
document.getElementById("Show-Pending-Reimbursement").addEventListener("click",showPendingReimbursement);
document.getElementById("Show-Approved-Reimbursement").addEventListener("click",showApprovedReimbursement);
document.getElementById("Show-Rejected-Reimbursement").addEventListener("click",showRejectedReimbursement);
document.getElementById("submit-btn").addEventListener("click",createNewReimbursement);

let reimbursementList2;

async function logout(){

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/project1/logout");
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4){
            console.log(localStorage);
            localStorage.removeItem("token");
            console.log(localStorage);
            window.location.href = "http://localhost:8080/project1/login.html"
        }
    }
    xhr.send();
  
}

function showEmployeeReimbursement(e){
    e.preventDefault();
    const xhr = new XMLHttpRequest();
    const token =  window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "ID";


    xhr.open("POST", "http://localhost:8080/project1/reimbursement2");
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4){
            let reimbursementList = JSON.parse(xhr.responseText);
            reimbursementList2 = JSON.parse(xhr.responseText);
            appendReimbursementToDom(reimbursementList);
        }
    }
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    const requestBody = `employee_id=${account[0]}&type=${type}`;
    console.log(requestBody);
    xhr.send(requestBody);
}
function showPendingReimbursement(e){
    e.preventDefault();
    console.log("click");
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "ID-PEN"

        xhr.open("POST", "http://localhost:8080/project1/reimbursement");
        xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                let reimbursementList = JSON.parse(xhr.responseText);
                reimbursementList2 = JSON.parse(xhr.responseText);
                appendReimbursementToDom(reimbursementList);
            }
        }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `employee_id=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);
}

function showApprovedReimbursement(e){
    e.preventDefault();
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "ID-APP"

        xhr.open("POST", "http://localhost:8080/project1/reimbursement");
        xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                let reimbursementList = JSON.parse(xhr.responseText);
                reimbursementList2 = JSON.parse(xhr.responseText);
                appendReimbursementToDom(reimbursementList);
            }
        }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `employee_id=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);

}

function showRejectedReimbursement(e){
    e.preventDefault();
    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "ID-REJ"

        xhr.open("POST", "http://localhost:8080/project1/reimbursement");
        xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                let reimbursementList = JSON.parse(xhr.responseText);
                reimbursementList2 = JSON.parse(xhr.responseText);
                appendReimbursementToDom(reimbursementList);
            }
        }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `employee_id=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);
}
function createNewReimbursement(e){
    e.preventDefault();
    const amount = document.getElementById("amount").value;
    const description = document.getElementById("description").value;

    const xhr = new XMLHttpRequest();
    const token = window.localStorage.getItem("token");
    const account = token.split(':');
    const type = "ID-CREATE"

    xhr.open("POST", "http://localhost:8080/project1/reimbursement");
    xhr.onreadystatechange = function(){
     if(xhr.readyState===4){
        }
     }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        const requestBody = `description=${description}&amount=${amount}&employee_id=${account[0]}&type=${type}`;
        console.log(requestBody);
        xhr.send(requestBody);
}

function appendReimbursementToDom(reimbursementList){
    document.getElementById('tbody').innerHTML = '';
    let reimbursementTable = document.getElementById("tbody");

    for (let reimbursement of reimbursementList) {
        let row = document.createElement("tr");
        let reimbursementId = document.createElement("td");
        reimbursementId.innerHTML = reimbursement.reimbursementId;
        row.appendChild(reimbursementId);
        let reimbursementAmount = document.createElement("td");
        reimbursementAmount.innerHTML = reimbursement.amount;
        row.appendChild(reimbursementAmount);
        let reimbursementInfo = document.createElement("td");
        reimbursementInfo.innerHTML = reimbursement.reimbursementInfo;
        row.appendChild(reimbursementInfo);
        let employee = document.createElement("td");
        employee.innerHTML = reimbursement.employee.employeeFName;
        row.appendChild(employee);
        let status = document.createElement("td");
        status.innerHTML = reimbursement.status;
        row.appendChild(status);
        reimbursementTable.appendChild(row);
    }
    console.log(reimbursementTable);

}

